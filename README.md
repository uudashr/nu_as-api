# API

The API

## Development

### Required Tools
- [Database migration tools](https://github.com/golang-migrate/migrate)
- [Mock generator](https://github.com/vektra/mockery)


### Database Migration
To create new database migration scripts
```
migrate create -ext sql -dir internal/mysql/migrations <name>
```

### Run Test

#### Unit Test
```
make test
```

#### Integration Test
```
make docker-mysql-up # wait until mysql up (around 5 seconds)

make test-integration

make docker-mysql-done # shutdown mysql
```