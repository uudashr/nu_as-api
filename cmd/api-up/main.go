package main

import (
	"flag"
	"fmt"
	"log"
	nethttp "net/http"
	"os"

	"bitbucket.org/uudashr/nu_as-api/internal/app"
	"bitbucket.org/uudashr/nu_as-api/internal/http"
	"bitbucket.org/uudashr/nu_as-api/internal/mysql"
	"bitbucket.org/uudashr/nu_as-api/internal/user"
)

func main() {
	flagDBUser := flag.String("db-user", "nu_as", "Database user")
	flagDBPassword := flag.String("db-password", "secret", "Database password")
	flagDBAddress := flag.String("db-address", "localhost:3306", "Database address")
	flagDBName := flag.String("db-name", "nu_as", "Database name")
	flagPort := flag.Int("port", 8080, "HTTP listen port")
	flagHelp := flag.Bool("help", false, "Show help (this message)")

	flag.Parse()

	if *flagHelp {
		flag.Usage()
		return
	}

	if *flagDBUser == "" || *flagDBPassword == "" || *flagDBAddress == "" || *flagDBName == "" {
		fmt.Fprintln(os.Stderr, "Require valid db-<config>")
		flag.Usage()
		os.Exit(1)
	}

	db, err := setupDB(*flagDBUser, *flagDBPassword, *flagDBAddress, *flagDBName)
	if err != nil {
		log.Fatal(err)
	}
	defer func() {
		if err = db.Close(); err != nil {
			log.Println("Fail to close db:", err)
		}
	}()

	accRepo, err := mysql.NewAccountRepository(db)
	if err != nil {
		log.Fatal(err)
	}

	pwdResetRepo, err := mysql.NewPasswordResetRepository(db)
	if err != nil {
		log.Fatal(err)
	}

	pwdService, err := user.NewPasswordService(pwdResetRepo, accRepo)
	if err != nil {
		log.Fatal(err)
	}

	appService, err := app.NewService(accRepo, pwdResetRepo, pwdService)
	if err != nil {
		log.Fatal(err)
	}

	handler, err := http.NewHandler(appService)
	if err != nil {
		log.Fatal(err)
	}

	server := &nethttp.Server{
		Handler: handler,
		Addr:    fmt.Sprintf(":%d", *flagPort),
	}

	log.Printf("Listening on port %d ...\n", *flagPort)
	if err = server.ListenAndServe(); err != nethttp.ErrServerClosed {
		log.Println("Fail to listen and serve:", err)
	}
}
