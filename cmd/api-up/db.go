package main

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/go-sql-driver/mysql"
)

func setupDB(user, password, address, name string) (*sql.DB, error) {
	log.Println("Connecting to db")
	dsn := fmt.Sprintf("%s:%s@tcp(%s)/%s?readTimeout=10s&timeout=10s&writeTimeout=10s&clientFoundRows=true&parseTime=true&loc=Local", user, password, address, name)
	db, err := sql.Open("mysql", dsn)
	if err != nil {
		return nil, err
	}

	if err = db.Ping(); err != nil {
		return nil, err
	}

	return db, nil
}
