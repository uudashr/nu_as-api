SOURCES := $(shell find . -name '*.go' -type f -not -path './vendor/*'  -not -path '*/mocks/*')

DB_USER ?= nu_as
DB_PASSWORD ?= secret
DB_PORT ?= 3306
DB_ADDRESS ?= 127.0.0.1:${DB_PORT}
DB_NAME ?= nu_as_test
DB_CONTAINER_NAME ?= nu_as-mysql

# Linter
.PHONY: prepare-lint
prepare-lint:
	@echo Install linter
	@curl -sfL https://install.goreleaser.com/github.com/golangci/golangci-lint.sh | sh -s -- -b $(go env GOPATH)/bin v1.17.1

.PHONY: lint
lint:
	@echo Lint checks
	@golangci-lint run $(LINT_OPTS)

# Dependency management
.PHONY: dep-upgrade
dep-upgrade:
	@echo Upgrading dependencies
	@go get -u

.PHONY: dep-tidy
dep-tidy:
	@echo Tidying dependencies
	@go mod tidy

# Generate files
.PHONY: generate
generate:
	@echo Generating files
	@go generate ./...

# MySQL
.PHONY: docker-mysql-up
docker-mysql-up:
	@docker run --rm -d --name $(DB_CONTAINER_NAME) -p ${DB_PORT}:3306 -e MYSQL_DATABASE=$(DB_NAME) -e MYSQL_USER=$(DB_USER) -e MYSQL_PASSWORD=$(DB_PASSWORD) -e MYSQL_ROOT_PASSWORD=rootsecret mysql:5 && docker logs -f $(DB_CONTAINER_NAME)

.PHONY: docker-mysql-down
docker-mysql-down:
	@docker stop $(DB_CONTAINER_NAME)

.PHONY: migrate-up
migrate-up:
	@migrate -database "mysql://$(DB_USER):$(DB_PASSWORD)@tcp($(DB_ADDRESS))/$(DB_NAME)?multiStatements=true" -path=internal/mysql/migrations up

.PHONY: migrate-down
migrate-down:
	@migrate -database "mysql://$(DB_USER):$(DB_PASSWORD)@tcp($(DB_ADDRESS))/$(DB_NAME)?multiStatements=true" -path=internal/mysql/migrations down

.PHONY: migrate-drop
migrate-drop:
	@migrate -database "mysql://$(DB_USER):$(DB_PASSWORD)@tcp($(DB_ADDRESS))/$(DB_NAME)?multiStatements=true" -path=internal/mysql/migrations drop

# Testing
.PHONY: test
test:
	@echo Run tests
	@go test $(TEST_OPTS) ./...

.PHONY: test-mysql
test-mysql:
	@echo Run mysql tests
	@go test -tags=integration $(TEST_OPTS) ./internal/mysql/...

.PHONY: test-integration
test-integration: test-mysql

test-all:
	@echo Run all tests
	@go test -tags=integration $(TEST_OPTS) ./...

# Build binary
api-up: cmd/$@ $(SOURCES)
	@echo "Building $@"
	@CGO_ENABLED=0 go build -a -installsuffix cgo -o $@ cmd/$@/*.go

# Build docker
.PHONY: docker
docker:
	@echo Building docker
	@docker build -t nuas-api .

# House keeping
.PHONY: clean
clean:
	@echo Cleaning...
	@if [ -f ./api-up ]; then rm ./api-up; fi
