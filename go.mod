module bitbucket.org/uudashr/nu_as-api

go 1.12

require (
	github.com/go-sql-driver/mysql v1.4.1
	github.com/golang-migrate/migrate/v4 v4.5.0
	github.com/gorilla/mux v1.7.3
	github.com/rs/xid v1.2.1
	github.com/stretchr/testify v1.3.0
	golang.org/x/crypto v0.0.0-20190701094942-4def268fd1a4
)
