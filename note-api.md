# API

## Sign Up

### Using Email & Password
```json
POST /accounts
{
    "email": "uudashr@gmail.com",
    "password": "secret"
}

Response "Created"
Location: /accounts/{id}
{
  "id": "an-account-id",
  "email": "uudashr@gmail.com"
}
```



### Using Google

```json
POST /accounts
{
  	"email": "uudashr@gmail.com",
    "googleId": "a-google-id"
}

Response "Created"
Location: /account/{id}
{
  "id": "an-account-id",
  "email": "uudashr@gmail.com",
  "googleId": "a-google-id"
}
```



## Init User Profile

```json
POST /accounts/{id}/init-profile
{
  "email": "uudashr@gmail.com",
  "fullName": "Nuruddin Ashr",
  "address": "Bogor, Indonesia",
  "phoneNumber": "+628176504656"
}

Response "See Other"
Location: /accounts/{id}/profile
```



## Get User Profile

```json
GET /accounts/{id}/profile

Response "OK"
{
  "email": "uudashr@gmail.com",
  "fullName": "Nuruddin Ashr",
  "address": "Bogor, Indonesia",
  "phoneNumber": "+628176504656"
}
```



## Update User Profile

```json
PUT /accounts/{id}/profile
{
  "email": "uudashr@gmail.com",
  "fullName": "Nuruddin Ashr",
  "address": "Bogor, Indonesia",
  "phoneNumber": "+628176504656"
}

Response "No Content"
```



## Forgot Password

### Request Password Reset

```json
POST /password-resets
{
  "email": "uudashr@gmail.com"
}

Response "Created"
Location: /password-resets/{id}
{
  "id": "a-reset-token",
  "accountId": "an-account-id",
  "expiryTime": "2019-07-21T16:43:15.744Z"
}
```



### Reset Password

```json
PUT /password-resets/{id}/password
"newsecret"

Response "No Content"
```

