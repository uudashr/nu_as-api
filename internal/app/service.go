package app

import (
	"context"
	"errors"
	"log"
	"time"

	"bitbucket.org/uudashr/nu_as-api/internal/core"

	"bitbucket.org/uudashr/nu_as-api/internal/eventd"

	"bitbucket.org/uudashr/nu_as-api/internal/user"
)

// Service represents the application service.
type Service struct {
	accRepo      user.AccountRepository
	pwdResetRepo user.PasswordResetRepository
	pwdService   *user.PasswordService
}

// NewService constructs new service instance.
func NewService(accRepo user.AccountRepository, pwdResetRepo user.PasswordResetRepository, pwdService *user.PasswordService) (*Service, error) {
	if accRepo == nil {
		return nil, errors.New("nil accRepo")
	}

	if pwdResetRepo == nil {
		return nil, errors.New("nil pwdResetRepo")
	}

	if pwdService == nil {
		return nil, errors.New("nil pwdService")
	}

	return &Service{
		accRepo:      accRepo,
		pwdResetRepo: pwdResetRepo,
		pwdService:   pwdService,
	}, nil
}

func (svc *Service) SignUpWithEmail(cmd SignUpWithEmailCommand) (*user.Account, error) {
	acc, err := user.CreateAccountWithEmail(user.NextAccountID(), cmd.Email, cmd.Password)
	if err != nil {
		return acc, nil
	}

	if err := svc.accRepo.Store(acc); err != nil {
		return nil, err
	}

	return acc, nil
}

func (svc *Service) SignUpWithGoogleID(cmd SignUpWithGoogleIDCommand) (*user.Account, error) {
	acc, err := user.CreateAccountWithGoogleID(user.NextAccountID(), cmd.Email, cmd.GoogleID)
	if err != nil {
		return acc, nil
	}

	if err := svc.accRepo.Store(acc); err != nil {
		return nil, err
	}

	return acc, nil
}

func (svc *Service) InitUserProfile(cmd InitUserProfileCommand) error {
	acc, err := svc.accRepo.AccountByID(cmd.AccountID)
	if err != nil {
		return err
	}

	if acc == nil {
		return core.ErrNotFound
	}

	if err := acc.ChangeFullName(cmd.FullName); err != nil {
		return err
	}

	if err := acc.ChangeAddress(cmd.Address); err != nil {
		return err
	}

	if err := acc.ChangePhoneNumber(cmd.PhoneNumber); err != nil {
		return err
	}

	return svc.accRepo.Update(acc)
}

func (svc *Service) UserProfile(accountID string) (*user.Account, error) {
	return svc.accRepo.AccountByID(accountID)
}

func (svc *Service) UpdateUserProfile(cmd UpdateUserProfileCommand) error {
	acc, err := svc.accRepo.AccountByID(cmd.AccountID)
	if err != nil {
		return err
	}

	if acc == nil {
		return core.ErrNotFound
	}

	if err := acc.ChangeFullName(cmd.FullName); err != nil {
		return err
	}

	if err := acc.ChangeAddress(cmd.Address); err != nil {
		return err
	}

	if err := acc.ChangeEmail(cmd.Email); err != nil {
		return err
	}

	if err := acc.ChangePhoneNumber(cmd.PhoneNumber); err != nil {
		return err
	}

	return svc.accRepo.Update(acc)
}

func (svc *Service) RequestPasswordReset(cmd RequestPasswordResetCommand) (*user.PasswordReset, error) {
	acc, err := svc.accRepo.AccountByEmail(cmd.Email)
	if err != nil {
		return nil, err
	}

	if acc == nil {
		return nil, core.ErrNotFound
	}

	// TODO: capture event to invalidate the others
	pwdReset, err := user.CreatePasswordReset(user.NextPasswordResetID(), acc.ID(), next24Hour())
	if err != nil {
		return nil, err
	}

	if err := svc.pwdResetRepo.Store(pwdReset); err != nil {
		return nil, err
	}

	return pwdReset, nil
}

func (svc *Service) ResetPassword(cmd ResetPasswordCommand) (retErr error) {
	lc, ctx := BeginLifeCycleWithContext(context.Background())
	defer lc.End(retErr)

	lc.Handler = EventHandlerFunc(func(events []eventd.Event) {
		for _, event := range events {
			switch v := event.(type) {
			case user.PasswordBeenReset:
				if err := svc.invalidatePasswordReset(v.Token); err != nil {
					log.Printf("Fail to invalidate password for token %q: %v\n", v.Token, err)
				}
			}
		}
	})

	acc, err := svc.pwdService.ResetPassword(ctx, cmd.ResetToken, cmd.Password)
	if err != nil {
		return err
	}

	return svc.accRepo.Store(acc)
}

func (svc *Service) invalidatePasswordReset(token string) error {
	pwdReset, err := svc.pwdResetRepo.PasswordResetByID(token)
	if err != nil {
		return err
	}

	if pwdReset == nil {
		return errors.New("invalid token")
	}

	if ok := pwdReset.Invalidate(); !ok {
		return errors.New("already invalidate")
	}

	return svc.pwdResetRepo.Update(pwdReset)
}

func next24Hour() time.Time {
	return time.Now().Add(24 * time.Hour)
}

type SignUpWithEmailCommand struct {
	Email    string
	Password string
}

type SignUpWithGoogleIDCommand struct {
	Email    string
	GoogleID string
}

type InitUserProfileCommand struct {
	AccountID   string
	FullName    string
	Address     string
	PhoneNumber string
}

type UpdateUserProfileCommand struct {
	AccountID   string
	FullName    string
	Address     string
	Email       string
	PhoneNumber string
}

type RequestPasswordResetCommand struct {
	Email string
}

type ResetPasswordCommand struct {
	ResetToken string
	Password   string
}
