package app_test

import (
	"testing"

	"bitbucket.org/uudashr/nu_as-api/internal/user"

	"github.com/stretchr/testify/mock"

	usermocks "bitbucket.org/uudashr/nu_as-api/internal/user/mocks"

	"bitbucket.org/uudashr/nu_as-api/internal/app"
)

type fixture struct {
	t            *testing.T
	accRepo      *usermocks.AccountRepository
	pwdResetRepo *usermocks.PasswordResetRepository
	pwdService   *user.PasswordService
	service      *app.Service
}

func setupFixture(t *testing.T) *fixture {
	accRepo := new(usermocks.AccountRepository)
	pwdResetRepo := new(usermocks.PasswordResetRepository)
	pwdService, err := user.NewPasswordService(pwdResetRepo, accRepo)
	if err != nil {
		t.Fatal("err:", err)
	}

	service, err := app.NewService(accRepo, pwdResetRepo, pwdService)
	if err != nil {
		t.Fatal("err:", err)
	}

	return &fixture{
		t:            t,
		accRepo:      accRepo,
		pwdResetRepo: pwdResetRepo,
		pwdService:   pwdService,
		service:      service,
	}
}

func (f *fixture) tearDown() {
	mock.AssertExpectationsForObjects(f.t,
		f.accRepo,
		f.pwdResetRepo)
}
