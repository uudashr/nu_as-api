package app

import (
	"context"

	"bitbucket.org/uudashr/nu_as-api/internal/eventd"
)

type LifeCycle struct {
	bus     *eventd.Bus
	Handler EventHandler
}

func BeginLifeCycle() *LifeCycle {
	return &LifeCycle{
		bus: new(eventd.Bus),
	}
}

func BeginLifeCycleWithContext(ctx context.Context) (*LifeCycle, context.Context) {
	bus := new(eventd.Bus)
	newCtx := eventd.ContextWithBus(ctx, bus)

	return &LifeCycle{
		bus: bus,
	}, newCtx
}

func (lc *LifeCycle) End(err error) {
	if lc.Handler == nil || err == nil {
		return
	}

	lc.Handler.HandleEvent(lc.bus.Events)
}

type EventHandler interface {
	HandleEvent([]eventd.Event)
}

type EventHandlerFunc func([]eventd.Event)

func (f EventHandlerFunc) HandleEvent(events []eventd.Event) {
	f(events)
}
