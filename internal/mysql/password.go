package mysql

import (
	"database/sql"
	"errors"
	"time"

	"bitbucket.org/uudashr/nu_as-api/internal/user"
)

type PasswordResetRepository struct {
	db *sql.DB
}

func NewPasswordResetRepository(db *sql.DB) (*PasswordResetRepository, error) {
	if db == nil {
		return nil, errors.New("nil db")
	}

	return &PasswordResetRepository{
		db: db,
	}, nil
}

func (repo *PasswordResetRepository) Store(rst *user.PasswordReset) error {
	res, err := repo.db.Exec("INSERT INTO password_resets (id, account_id, invalidated, expiry_time) VALUES (?, ?, ?, ?)",
		rst.ID(),
		rst.AccountID(),
		rst.Invalidated(),
		rst.ExpiryTime())
	if err != nil {
		return err
	}

	affected, err := res.RowsAffected()
	if err != nil {
		return err
	}

	if affected == 0 {
		return errors.New("no rows affected")
	}

	return nil
}

func (repo *PasswordResetRepository) PasswordResetByID(id string) (*user.PasswordReset, error) {
	var (
		accountID   string
		invalidated bool
		expiryTime  time.Time
	)

	if err := repo.db.QueryRow("SELECT account_id, invalidated, expiry_time FROM password_resets WHERE id = ?", id).Scan(
		&accountID,
		&invalidated,
		&expiryTime,
	); err != nil {
		return nil, err
	}

	return user.NewPasswordReset(id, accountID, invalidated, expiryTime)
}

func (repo *PasswordResetRepository) Update(rst *user.PasswordReset) error {
	res, err := repo.db.Exec("UPDATE password_resets SET invalidated = ? WHERE id = ?",
		rst.Invalidated(),
		rst.ID())
	if err != nil {
		return err
	}

	affected, err := res.RowsAffected()
	if err != nil {
		return err
	}

	if affected == 0 {
		return errors.New("no rows affected")
	}

	return nil
}
