// +build integration

package mysql_test

import (
	"time"
	"bitbucket.org/uudashr/nu_as-api/internal/user"
	"testing"

	"bitbucket.org/uudashr/nu_as-api/internal/mysql"
)

func TestPasswordResetRepository_RetrieveStored(t *testing.T) {
	fix := setupPasswordResetFixture(t)
	defer fix.tearDown()

	pwdReset, err := user.CreatePasswordReset(user.NextPasswordResetID(), "an-account-id", time.Now().Add(24 * time.Hour))
	if err != nil {
		t.Fatal("err:", err)
	}

	if err := fix.repo.Store(pwdReset); err != nil {
		t.Fatal("err:", err)
	}

	// By ID
	retPwdReset, err := fix.repo.PasswordResetByID(pwdReset.ID())
	if err != nil {
		t.Fatal("err:", err)
	}

	if got, want := retPwdReset.ID(), pwdReset.ID(); got != want {
		t.Errorf("ID got: %q, want:%q", got, want)
	}

	if got, want := retPwdReset.AccountID(), pwdReset.AccountID(); got != want {
		t.Errorf("AccountID got: %q, want:%q", got, want)
	}

	if got, want := retPwdReset.Invalidated(), pwdReset.Invalidated(); got != want {
		t.Errorf("Invalidated got: %t, want:%t", got, want)
	}

	if got, want := retPwdReset.ExpiryTime().Round(time.Second), pwdReset.ExpiryTime().Round(time.Second); !got.Equal(want) {
		t.Errorf("ExpiryTime got: %v, want:%v", got, want)
	}
}

func TestPasswordResetRepository_Update(t *testing.T) {
	fix := setupPasswordResetFixture(t)
	defer fix.tearDown()

	pwdReset, err := user.CreatePasswordReset(user.NextPasswordResetID(), "an-account-id", time.Now().Add(24 * time.Hour))
	if err != nil {
		t.Fatal("err:", err)
	}

	if err := fix.repo.Store(pwdReset); err != nil {
		t.Fatal("err:", err)
	}

	// Change some details
	_ = pwdReset.Invalidate()
	if err := fix.repo.Update(pwdReset); err != nil {
		t.Fatal("err:", err)
	}

	// Assert
	retPwdReset, err := fix.repo.PasswordResetByID(pwdReset.ID())
	if err != nil {
		t.Fatal("err:", err)
	}

	if got, want := retPwdReset.ID(), pwdReset.ID(); got != want {
		t.Errorf("ID got: %q, want:%q", got, want)
	}

	if got, want := retPwdReset.AccountID(), pwdReset.AccountID(); got != want {
		t.Errorf("AccountID got: %q, want:%q", got, want)
	}

	if got, want := retPwdReset.Invalidated(), true; got != want {
		t.Errorf("Invalidated got: %t, want:%t", got, want)
	}

	if got, want := retPwdReset.ExpiryTime().Round(time.Second), pwdReset.ExpiryTime().Round(time.Second); !got.Equal(want) {
		t.Errorf("ExpiryTime got: %v, want:%v", got, want)
	}
}

type passwordResetFixture struct {
	dbFix *dbFixture
	repo  *mysql.PasswordResetRepository
}

func setupPasswordResetFixture(t *testing.T) *passwordResetFixture {
	dbFix := setupDBFixture(t)
	repo, err := mysql.NewPasswordResetRepository(dbFix.db)
	if err != nil {
		t.Fatal("err:", err)
	}

	_ = dbFix
	return &passwordResetFixture{
		dbFix: dbFix,
		repo:  repo,
	}
}

func (fix *passwordResetFixture) tearDown() {
	fix.dbFix.tearDown()
}
