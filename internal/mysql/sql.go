package mysql

import (
	"database/sql"
)

func nullableBytes(b []byte) sql.NullString {
	if b == nil {
		return sql.NullString{Valid: false}
	}

	return sql.NullString{String: string(b), Valid: true}
}

func nullableString(s string) sql.NullString {
	if s == "" {
		return sql.NullString{}
	}

	return sql.NullString{String: s, Valid: true}
}
