// +build integration

package mysql_test

import (
	"bitbucket.org/uudashr/nu_as-api/internal/user"
	"bitbucket.org/uudashr/nu_as-api/internal/mysql"
	"testing"
)

func TestAccountRepository_RetrieveStoredAccount(t *testing.T) {
	fix := setupAccountFixture(t)
	defer fix.tearDown()

	acc, err := user.NewAccount(user.NextAccountID(), "uudashr@gmail.com", nil, "Nuruddin Ashr", "Bogor, Indonesia", "+628176504656", "")
	if err != nil {
		t.Fatal("err:", err)
	}

	if err := fix.repo.Store(acc); err != nil {
		t.Fatal("err:", err)
	}


	// By ID
	retAcc, err := fix.repo.AccountByID(acc.ID())
	if err != nil {
		t.Fatal("err:", err)
	}

	if got, want := retAcc.ID(), acc.ID(); got != want {
		t.Errorf("ID got: %q, want: %q", got, want)
	}

	// By Email
	retAccByEmail, err := fix.repo.AccountByEmail(acc.Email())
	if err != nil {
		t.Fatal("err:", err)
	}

	if got, want := retAccByEmail.ID(), acc.ID(); got != want {
		t.Errorf("ID got: %q, want: %q", got, want)
	}
}

func TestAccountRepository_UpdateAccount(t *testing.T) {
	fix := setupAccountFixture(t)
	defer fix.tearDown()

	acc, err := user.NewAccount(user.NextAccountID(), "uudashr@gmail.com", nil, "Nuruddin Ashr", "Bogor, Indonesia", "+628176504656", "")
	if err != nil {
		t.Fatal("err:", err)
	}

	if err := fix.repo.Store(acc); err != nil {
		t.Fatal("err:", err)
	}


	// Change some details
	if err := acc.ChangeFullName("Groot"); err != nil {
		t.Fatal("err:", err)
	}

	if err := fix.repo.Update(acc); err != nil {
		t.Fatal("err:", err)
	}

	// Assert
	retAcc, err := fix.repo.AccountByID(acc.ID())
	if err != nil {
		t.Fatal("err:", err)
	}

	if got, want := retAcc.FullName(), acc.FullName(); got != want {
		t.Errorf("FullName got: %q, want: %q", got, want)
	}
}

type accountFixture struct {
	dbFix *dbFixture
	repo *mysql.AccountRepository
}

func setupAccountFixture(t *testing.T) *accountFixture {
	dbFix := setupDBFixture(t)
	repo, err := mysql.NewAccountRepository(dbFix.db)
	if err != nil {
		t.Fatal("err:", err)
	}

	_ = dbFix
	return &accountFixture{
		dbFix: dbFix,
		repo: repo,
	}
}

func (fix *accountFixture) tearDown() {
	fix.dbFix.tearDown()
}
