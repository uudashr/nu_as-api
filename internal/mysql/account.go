package mysql

import (
	"database/sql"
	"errors"

	"bitbucket.org/uudashr/nu_as-api/internal/user"
)

type AccountRepository struct {
	db *sql.DB
}

func NewAccountRepository(db *sql.DB) (*AccountRepository, error) {
	if db == nil {
		return nil, errors.New("nil db")
	}

	return &AccountRepository{
		db: db,
	}, nil
}

func (repo *AccountRepository) Store(acc *user.Account) error {
	res, err := repo.db.Exec("INSERT INTO accounts (id, email, hashed_password, full_name, address, phone_number, google_id) VALUES (?, ?, ?, ?, ?, ?, ?)",
		acc.ID(),
		acc.Email(),
		nullableBytes(acc.HashedPassword()),
		nullableString(acc.FullName()),
		nullableString(acc.Address()),
		nullableString(acc.PhoneNumber()),
		nullableString(acc.GoogleID()))
	if err != nil {
		return err
	}

	affected, err := res.RowsAffected()
	if err != nil {
		return err
	}

	if affected == 0 {
		return errors.New("no rows affected")
	}

	return nil
}

func (repo *AccountRepository) AccountByID(id string) (*user.Account, error) {
	var (
		email          string
		hashedPassword sql.NullString
		fullName       sql.NullString
		address        sql.NullString
		phoneNumber    sql.NullString
		googleID       sql.NullString
	)

	if err := repo.db.QueryRow("SELECT email, hashed_password, full_name, address, phone_number, google_id FROM accounts WHERE id = ?", id).Scan(
		&email,
		&hashedPassword,
		&fullName,
		&address,
		&phoneNumber,
		&googleID,
	); err != nil {
		return nil, err
	}

	var hashedPwd []byte
	if hashedPassword.Valid {
		hashedPwd = []byte(hashedPassword.String)
	}

	return user.NewAccount(id, email, hashedPwd, fullName.String, address.String, phoneNumber.String, googleID.String)
}

func (repo *AccountRepository) AccountByEmail(email string) (*user.Account, error) {
	var (
		id             string
		hashedPassword sql.NullString
		fullName       sql.NullString
		address        sql.NullString
		phoneNumber    sql.NullString
		googleID       sql.NullString
	)

	if err := repo.db.QueryRow("SELECT id, hashed_password, full_name, address, phone_number, google_id FROM accounts WHERE email = ?", email).Scan(
		&id,
		&hashedPassword,
		&fullName,
		&address,
		&phoneNumber,
		&googleID,
	); err != nil {
		return nil, err
	}

	var hashedPwd []byte
	if hashedPassword.Valid {
		hashedPwd = []byte(hashedPassword.String)
	}

	return user.NewAccount(id, email, hashedPwd, fullName.String, address.String, phoneNumber.String, googleID.String)
}

func (repo *AccountRepository) Update(acc *user.Account) error {
	res, err := repo.db.Exec("UPDATE accounts SET email = ?, hashed_password = ?, full_name = ?, address = ?, phone_number = ?, google_id = ? WHERE id = ?",
		acc.Email(),
		nullableBytes(acc.HashedPassword()),
		nullableString(acc.FullName()),
		nullableString(acc.Address()),
		nullableString(acc.PhoneNumber()),
		nullableString(acc.GoogleID()),
		acc.ID())
	if err != nil {
		return err
	}

	affected, err := res.RowsAffected()
	if err != nil {
		return err
	}

	if affected == 0 {
		return errors.New("no rows affected")
	}

	return nil
}
