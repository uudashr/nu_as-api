CREATE TABLE accounts (
    id VARCHAR(20) NOT NULL,
    email VARCHAR(254) NOT NULL,
    hashed_password VARCHAR(60),
    full_name VARCHAR(100),
    address TEXT,
    phone_number VARCHAR(50),
    google_id VARCHAR(50),
    PRIMARY KEY(id), 
    CONSTRAINT uc_email UNIQUE(email)
) ENGINE=InnoDB;
-- created_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
-- updated_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,