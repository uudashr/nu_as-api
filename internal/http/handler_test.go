package http_test

import (
	"encoding/json"
	"fmt"
	nethttp "net/http"
	"strings"
	"testing"
	"time"

	"bitbucket.org/uudashr/nu_as-api/internal/app"
	"bitbucket.org/uudashr/nu_as-api/internal/user"

	"bitbucket.org/uudashr/nu_as-api/internal/http/mocks"
	"github.com/stretchr/testify/mock"

	"bitbucket.org/uudashr/nu_as-api/internal/http"
)

func TestSignUpWithEmail(t *testing.T) {
	fix := setupFixture(t)
	defer fix.tearDown()

	email := "uudashr@gmail.com"
	password := "secret"

	acc, err := user.CreateAccountWithEmail(user.NextAccountID(), email, password)
	if err != nil {
		t.Fatal("err:", err)
	}

	fix.appService.On("SignUpWithEmail", app.SignUpWithEmailCommand{
		Email:    acc.Email(),
		Password: password,
	}).Return(acc, nil)

	res := httpPost(fix.handler, "/accounts", map[string]interface{}{
		"email":    acc.Email(),
		"password": password,
		"hint":     "email",
	})
	defer res.Body.Close()

	if got, want := res.StatusCode, nethttp.StatusCreated; got != want {
		t.Fatalf("StatusCode got: %d, want: %d", got, want)
	}

	if got, wantPrefix := res.Header.Get("Content-Type"), "application/json"; !strings.HasPrefix(got, wantPrefix) {
		t.Fatalf("ContentType got: %q, wantPrefix: %q", got, wantPrefix)
	}

	var resPayload accountPayload
	if err := json.NewDecoder(res.Body).Decode(&resPayload); err != nil {
		t.Fatal("err:", err)
	}

	if got, want := resPayload.ID, acc.ID(); got != want {
		t.Errorf("ID got: %q, want: %q", got, want)
	}

	if got, want := resPayload.Email, acc.Email(); got != want {
		t.Errorf("Email got: %q, want: %q", got, want)
	}

	if got, want := resPayload.GoogleID, acc.GoogleID(); got != want {
		t.Errorf("GoogleID got: %q, want: %q", got, want)
	}
}

func TestSignUpWithGoogleID(t *testing.T) {
	fix := setupFixture(t)
	defer fix.tearDown()

	email := "uudashr@gmail.com"
	googleID := "a-google-id"

	acc, err := user.CreateAccountWithGoogleID(user.NextAccountID(), email, googleID)
	if err != nil {
		t.Fatal("err:", err)
	}

	fix.appService.On("SignUpWithGoogleID", app.SignUpWithGoogleIDCommand{
		Email:    acc.Email(),
		GoogleID: acc.GoogleID(),
	}).Return(acc, nil)

	res := httpPost(fix.handler, "/accounts", map[string]interface{}{
		"email":    acc.Email(),
		"googleId": acc.GoogleID(),
		"hint":     "google",
	})
	defer res.Body.Close()

	if got, want := res.StatusCode, nethttp.StatusCreated; got != want {
		t.Fatalf("StatusCode got: %d, want: %d", got, want)
	}

	if got, wantPrefix := res.Header.Get("Content-Type"), "application/json"; !strings.HasPrefix(got, wantPrefix) {
		t.Fatalf("ContentType got: %q, wantPrefix: %q", got, wantPrefix)
	}

	var resPayload accountPayload
	if err := json.NewDecoder(res.Body).Decode(&resPayload); err != nil {
		t.Fatal("err:", err)
	}

	if got, want := resPayload.ID, acc.ID(); got != want {
		t.Errorf("ID got: %q, want: %q", got, want)
	}

	if got, want := resPayload.Email, acc.Email(); got != want {
		t.Errorf("Email got: %q, want: %q", got, want)
	}

	if got, want := resPayload.GoogleID, acc.GoogleID(); got != want {
		t.Errorf("GoogleID got: %q, want: %q", got, want)
	}
}

func TestInitUserProfile(t *testing.T) {
	fix := setupFixture(t)
	defer fix.tearDown()

	id := "an-account-id"
	fullName := "Nuruddin Ashr"
	address := "Bogor, Indonesia"
	phoneNumber := "+628176504656"

	fix.appService.On("InitUserProfile", app.InitUserProfileCommand{
		AccountID:   id,
		FullName:    fullName,
		Address:     address,
		PhoneNumber: phoneNumber,
	}).Return(nil)

	res := httpPost(fix.handler, fmt.Sprintf("/accounts/%s/init-profile", id), map[string]interface{}{
		"fullName":    fullName,
		"address":     address,
		"phoneNumber": phoneNumber,
	})
	defer res.Body.Close()

	if got, want := res.StatusCode, nethttp.StatusSeeOther; got != want {
		t.Fatalf("StatusCode got: %d, want: %d", got, want)
	}
}

func TestUserProfile(t *testing.T) {
	fix := setupFixture(t)
	defer fix.tearDown()

	acc, err := user.NewAccount(user.NextAccountID(), "uudashr@gmail.com", nil, "Nuruddin Ashr", "Bogor, Indonesia", "+628176504656", "")
	if err != nil {
		t.Fatal("err:", err)
	}

	fix.appService.On("UserProfile", acc.ID()).Return(acc, nil)

	res := httpGet(fix.handler, fmt.Sprintf("/accounts/%s/profile", acc.ID()))
	defer res.Body.Close()

	if got, want := res.StatusCode, nethttp.StatusOK; got != want {
		t.Fatalf("StatusCode got: %d, want: %d", got, want)
	}

	if got, wantPrefix := res.Header.Get("Content-Type"), "application/json"; !strings.HasPrefix(got, wantPrefix) {
		t.Fatalf("ContentType got: %q, wantPrefix: %q", got, wantPrefix)
	}

	var resPayload profilePayload
	if err := json.NewDecoder(res.Body).Decode(&resPayload); err != nil {
		t.Fatal("err:", err)
	}

	if got, want := resPayload.FullName, acc.FullName(); got != want {
		t.Errorf("FullName got: %q, want: %q", got, want)
	}

	if got, want := resPayload.Address, acc.Address(); got != want {
		t.Errorf("Address got: %q, want: %q", got, want)
	}

	if got, want := resPayload.Email, acc.Email(); got != want {
		t.Errorf("Email got: %q, want: %q", got, want)
	}

	if got, want := resPayload.PhoneNumber, acc.PhoneNumber(); got != want {
		t.Errorf("PhoneNumber got: %q, want: %q", got, want)
	}
}

func TestUpdateUserProfile(t *testing.T) {
	fix := setupFixture(t)
	defer fix.tearDown()

	id := "an-account-id"
	email := "uudashr@gmail.com"
	fullName := "Nuruddin Ashr"
	address := "Bogor, Indonesia"
	phoneNumber := "+628176504656"

	fix.appService.On("UpdateUserProfile", app.UpdateUserProfileCommand{
		AccountID:   id,
		FullName:    fullName,
		Address:     address,
		Email:       email,
		PhoneNumber: phoneNumber,
	}).Return(nil)

	res := httpPut(fix.handler, fmt.Sprintf("/accounts/%s/profile", id), map[string]interface{}{
		"fullName":    fullName,
		"address":     address,
		"email":       email,
		"phoneNumber": phoneNumber,
	})
	defer res.Body.Close()

	if got, want := res.StatusCode, nethttp.StatusNoContent; got != want {
		t.Fatalf("StatusCode got: %d, want: %d", got, want)
	}
}

func TestRequestPasswordReset(t *testing.T) {
	fix := setupFixture(t)
	defer fix.tearDown()

	email := "uudashr@gmail.com"
	pwdReset, err := user.CreatePasswordReset(user.NextPasswordResetID(), "an-token", time.Now().Add(24*time.Hour))
	if err != nil {
		t.Fatal("err:", err)
	}

	fix.appService.On("RequestPasswordReset", app.RequestPasswordResetCommand{
		Email: email,
	}).Return(pwdReset, nil)

	res := httpPost(fix.handler, "/password-resets", map[string]interface{}{
		"email": email,
	})
	defer res.Body.Close()

	if got, want := res.StatusCode, nethttp.StatusCreated; got != want {
		t.Fatalf("StatusCode got: %d, want: %d", got, want)
	}

	var resPayload passwordResetPayload
	if err := json.NewDecoder(res.Body).Decode(&resPayload); err != nil {
		t.Fatal("err:", err)
	}

	if got, want := resPayload.ID, pwdReset.ID(); got != want {
		t.Errorf("ID got: %q, want: %q", got, want)
	}

	if got, want := resPayload.AccountID, pwdReset.AccountID(); got != want {
		t.Errorf("AccountID got: %q, want: %q", got, want)
	}
}

func TestResetPassword(t *testing.T) {
	fix := setupFixture(t)
	defer fix.tearDown()

	resetID := "a-token"
	password := "newsecret"

	fix.appService.On("ResetPassword", app.ResetPasswordCommand{
		ResetToken: resetID,
		Password:   password,
	}).Return(nil)

	res := httpPut(fix.handler, fmt.Sprintf("/password-resets/%s/password", resetID), password)
	defer res.Body.Close()

	if got, want := res.StatusCode, nethttp.StatusNoContent; got != want {
		t.Fatalf("StatusCode got: %d, want: %d", got, want)
	}
}

func TestHealthz(t *testing.T) {
	fix := setupFixture(t)
	defer fix.tearDown()

	res := httpGet(fix.handler, "/healthz")
	if got, want := res.StatusCode, nethttp.StatusOK; got != want {
		t.Fatalf("StatusCode got: %d, want: %d", got, want)
	}
	defer res.Body.Close()
}

type fixture struct {
	t          *testing.T
	appService *mocks.AppService
	handler    nethttp.Handler
}

func setupFixture(t *testing.T) *fixture {
	appService := new(mocks.AppService)
	handler, err := http.NewHandler(appService)
	if err != nil {
		t.Fatal("err:", err)
	}

	return &fixture{
		t:          t,
		appService: appService,
		handler:    handler,
	}
}

func (f *fixture) tearDown() {
	mock.AssertExpectationsForObjects(f.t,
		f.appService)
}

type accountPayload struct {
	ID       string `json:"id"`
	Email    string `json:"email"`
	GoogleID string `json:"googleId"`
}

type profilePayload struct {
	FullName    string `json:"fullName"`
	Address     string `json:"address"`
	Email       string `json:"email"`
	PhoneNumber string `json:"phoneNumber"`
}

type passwordResetPayload struct {
	ID         string    `json:"id"`
	AccountID  string    `json:"accountId"`
	ExpiryTime time.Time `json:"expiryTime"`
}
