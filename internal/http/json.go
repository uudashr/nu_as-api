package http

import (
	"encoding/json"
	"net/http"
)

func writeJSON(w http.ResponseWriter, code int, i interface{}) error {
	b, err := json.Marshal(i)
	if err != nil {
		return err
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	if _, err = w.Write(b); err != nil {
		return err
	}

	return nil
}
