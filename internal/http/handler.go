package http

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"time"

	"bitbucket.org/uudashr/nu_as-api/internal/core"

	"bitbucket.org/uudashr/nu_as-api/internal/app"
	"bitbucket.org/uudashr/nu_as-api/internal/user"
	"github.com/gorilla/mux"
)

// AppService interface for app.Service.
//go:generate mockery -name=AppService
type AppService interface {
	SignUpWithEmail(app.SignUpWithEmailCommand) (*user.Account, error)
	SignUpWithGoogleID(app.SignUpWithGoogleIDCommand) (*user.Account, error)
	InitUserProfile(app.InitUserProfileCommand) error
	UserProfile(accountID string) (*user.Account, error)
	UpdateUserProfile(app.UpdateUserProfileCommand) error
	RequestPasswordReset(app.RequestPasswordResetCommand) (*user.PasswordReset, error)
	ResetPassword(app.ResetPasswordCommand) error
}

type delegate struct {
	appService AppService
}

func (d *delegate) createAccount(w http.ResponseWriter, r *http.Request) {
	// TODO: ensure application/json
	var reqPayload createAccountPayload
	if err := json.NewDecoder(r.Body).Decode(&reqPayload); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	var (
		acc *user.Account
		err error
	)
	switch reqPayload.Hint {
	case "email":
		acc, err = d.appService.SignUpWithEmail(app.SignUpWithEmailCommand{
			Email:    reqPayload.Email,
			Password: reqPayload.Password,
		})
	case "google":
		acc, err = d.appService.SignUpWithGoogleID(app.SignUpWithGoogleIDCommand{
			Email:    reqPayload.Email,
			GoogleID: reqPayload.GoogleID,
		})
	default:
		http.Error(w, "unknown hint", http.StatusBadRequest)
		return
	}

	if err != nil {
		if _, ok := err.(core.ValidationError); ok {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Location", fmt.Sprintf("/accounts/%s", acc.ID()))
	if err := writeJSON(w, http.StatusCreated, accountPayload{
		ID:       acc.ID(),
		Email:    acc.Email(),
		GoogleID: acc.GoogleID(),
	}); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
}

func (d *delegate) initUserProfile(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	varID := vars["id"]

	var reqPayload initProfilePayload
	if err := json.NewDecoder(r.Body).Decode(&reqPayload); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	if err := d.appService.InitUserProfile(app.InitUserProfileCommand{
		AccountID:   varID,
		FullName:    reqPayload.FullName,
		Address:     reqPayload.Address,
		PhoneNumber: reqPayload.PhoneNumber,
	}); err != nil {
		if err == core.ErrNotFound {
			http.NotFound(w, r)
			return
		}

		if _, ok := err.(core.ValidationError); ok {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Location", fmt.Sprintf("/accounts/%s/profile", varID))
	w.WriteHeader(http.StatusSeeOther)
}

func (d *delegate) userProfile(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	varID := vars["id"]

	acc, err := d.appService.UserProfile(varID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if acc == nil {
		http.NotFound(w, r)
		return
	}

	if err := writeJSON(w, http.StatusOK, profilePayload{
		FullName:    acc.FullName(),
		Address:     acc.Address(),
		Email:       acc.Email(),
		PhoneNumber: acc.PhoneNumber(),
	}); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (d *delegate) updateUserProfile(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	varID := vars["id"]

	var reqPayload updateProfilePayload
	if err := json.NewDecoder(r.Body).Decode(&reqPayload); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	if err := d.appService.UpdateUserProfile(app.UpdateUserProfileCommand{
		AccountID:   varID,
		FullName:    reqPayload.FullName,
		Address:     reqPayload.Address,
		Email:       reqPayload.Email,
		PhoneNumber: reqPayload.PhoneNumber,
	}); err != nil {
		if err == core.ErrNotFound {
			http.NotFound(w, r)
			return
		}

		if _, ok := err.(core.ValidationError); ok {
			http.Error(w, err.Error(), http.StatusBadRequest)
		}

		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

func (d *delegate) requestPasswordReset(w http.ResponseWriter, r *http.Request) {
	var reqPayload requestPasswordResetPayload
	if err := json.NewDecoder(r.Body).Decode(&reqPayload); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	pwdReset, err := d.appService.RequestPasswordReset(app.RequestPasswordResetCommand{
		Email: reqPayload.Email,
	})
	if err != nil {
		if err == core.ErrNotFound {
			http.NotFound(w, r)
			return
		}

		if _, ok := err.(core.ValidationError); ok {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Location", fmt.Sprintf("/password-resets/%s", pwdReset.ID()))
	if err := writeJSON(w, http.StatusCreated, passwordResetPayload{
		ID:         pwdReset.ID(),
		AccountID:  pwdReset.AccountID(),
		ExpiryTime: pwdReset.ExpiryTime(),
	}); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func (d *delegate) resetPassword(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	varID := vars["id"]

	var password string
	if err := json.NewDecoder(r.Body).Decode(&password); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	if err := d.appService.ResetPassword(app.ResetPasswordCommand{
		ResetToken: varID,
		Password:   password,
	}); err != nil {
		if _, ok := err.(core.ValidationError); ok {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

func (d *delegate) healthz(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
}

// NewHandler constructs new http.Handler.
func NewHandler(appService AppService) (http.Handler, error) {
	if appService == nil {
		return nil, errors.New("nil appService")
	}

	d := &delegate{
		appService: appService,
	}

	r := mux.NewRouter()
	r.HandleFunc("/accounts", d.createAccount).Methods("POST")
	r.HandleFunc("/accounts/{id}/init-profile", d.initUserProfile).Methods("POST")
	r.HandleFunc("/accounts/{id}/profile", d.userProfile).Methods("GET")
	r.HandleFunc("/accounts/{id}/profile", d.updateUserProfile).Methods("PUT")

	r.HandleFunc("/password-resets", d.requestPasswordReset).Methods("POST")
	r.HandleFunc("/password-resets/{id}/password", d.resetPassword).Methods("PUT")

	r.HandleFunc("/healthz", d.healthz).Methods("GET")

	return r, nil
}

type createAccountPayload struct {
	Email    string `json:"email"`
	Password string `json:"password"`
	GoogleID string `json:"googleId"`
	Hint     string `json:"hint"`
}

type accountPayload struct {
	ID       string `json:"id"`
	Email    string `json:"email"`
	GoogleID string `json:"googleId,omitempty"`
}

type initProfilePayload struct {
	FullName    string `json:"fullName"`
	Address     string `json:"address"`
	PhoneNumber string `json:"phoneNumber"`
}

type profilePayload struct {
	FullName    string `json:"fullName,omitempty"`
	Address     string `json:"address,omitempty"`
	Email       string `json:"email"`
	PhoneNumber string `json:"phoneNumber,omitempty"`
}

type updateProfilePayload struct {
	FullName    string `json:"fullName"`
	Address     string `json:"address"`
	Email       string `json:"email"`
	PhoneNumber string `json:"phoneNumber"`
}

type requestPasswordResetPayload struct {
	Email string `json:"email"`
}

type passwordResetPayload struct {
	ID         string    `json:"id"`
	AccountID  string    `json:"accountId"`
	ExpiryTime time.Time `json:"expiryTime"`
}
