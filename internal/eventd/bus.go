package eventd

import "context"

type contextKey int

const (
	contextKeyBus contextKey = iota
)

type Event interface{}

type Publisher interface {
	Publish(Event)
}

func PublishContext(ctx context.Context, e Event) {
	bus := BusFromContext(ctx)
	if bus == nil {
		return
	}

	bus.Publish(e)
}

func ContextWithBus(parent context.Context, bus *Bus) context.Context {
	return context.WithValue(parent, contextKeyBus, bus)
}

func BusFromContext(ctx context.Context) *Bus {
	bus, ok := ctx.Value(contextKeyBus).(*Bus)
	if !ok {
		return nil
	}

	return bus
}

type Bus struct {
	Events []Event
}

func (b *Bus) Publish(e Event) {
	b.Events = append(b.Events, e)
}
