package core

import (
	"errors"
)

// ErrNotFound errors reaised when reference not found.
var ErrNotFound = errors.New("not found")

// ValidationError errors caused by validation.
type ValidationError string

func (err ValidationError) Error() string {
	return string(err)
}
