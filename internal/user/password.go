package user

import (
	"context"
	"errors"
	"time"

	"bitbucket.org/uudashr/nu_as-api/internal/core"

	"bitbucket.org/uudashr/nu_as-api/internal/eventd"

	"github.com/rs/xid"
)

type PasswordReset struct {
	id          string
	accountID   string
	invalidated bool
	expiryTime  time.Time
}

func NewPasswordReset(id, accountID string, invalidated bool, expiryTime time.Time) (*PasswordReset, error) {
	if id == "" {
		return nil, core.ValidationError("empty id")
	}

	if accountID == "" {
		return nil, core.ValidationError("empty accountID")
	}

	if expiryTime.IsZero() {
		return nil, core.ValidationError("zero expiryTime")
	}

	return &PasswordReset{
		id:          id,
		accountID:   accountID,
		invalidated: invalidated,
		expiryTime:  expiryTime,
	}, nil
}

func CreatePasswordReset(id, accountID string, expiryTime time.Time) (*PasswordReset, error) {
	// TODO: publish event
	return NewPasswordReset(id, accountID, false, expiryTime)
}

func (rst PasswordReset) ID() string {
	return rst.id
}

func (rst PasswordReset) AccountID() string {
	return rst.accountID
}

func (rst PasswordReset) Invalidated() bool {
	return rst.invalidated
}

func (rst *PasswordReset) Invalidate() bool {
	if rst.invalidated {
		return false
	}

	rst.invalidated = true
	return true
}

func (rst PasswordReset) ExpiryTime() time.Time {
	return rst.expiryTime
}

func (rst PasswordReset) Expired(now time.Time) bool {
	return now.Before(rst.expiryTime)
}

func (rst PasswordReset) Valid(now time.Time) bool {
	return rst.Invalidated() || rst.Expired(time.Now())
}

func NextPasswordResetID() string {
	return xid.New().String()
}

// PasswordResetRepository represents passwod reset repository.
//go:generate mockery -name=PasswordResetRepository
type PasswordResetRepository interface {
	Store(*PasswordReset) error
	PasswordResetByID(id string) (*PasswordReset, error)
	Update(*PasswordReset) error
}

type PasswordService struct {
	repo    PasswordResetRepository
	accRepo AccountRepository
}

func NewPasswordService(repo PasswordResetRepository, accRepo AccountRepository) (*PasswordService, error) {
	if repo == nil {
		return nil, errors.New("nil repo")
	}

	if accRepo == nil {
		return nil, errors.New("nil accRepo")
	}

	return &PasswordService{
		repo:    repo,
		accRepo: accRepo,
	}, nil
}

func (svc *PasswordService) ResetPassword(ctx context.Context, token, password string) (*Account, error) {
	pwdReset, err := svc.repo.PasswordResetByID(token)
	if err != nil {
		return nil, err
	}

	if pwdReset == nil || pwdReset.Valid(time.Now()) {
		return nil, core.ValidationError("invalid resetToken")
	}

	acc, err := svc.accRepo.AccountByID(pwdReset.AccountID())
	if err != nil {
		return nil, err
	}

	if acc == nil {
		return nil, core.ValidationError("invalid resetToken")
	}

	if err := acc.ChangePassword(password); err != nil {
		return nil, err
	}

	eventd.PublishContext(ctx, PasswordBeenReset{
		Token:     token,
		AccountID: acc.ID(),
	})

	return acc, nil
}

type PasswordBeenReset struct {
	Token     string
	AccountID string
}
