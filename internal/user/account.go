package user

import (
	"bitbucket.org/uudashr/nu_as-api/internal/core"

	"github.com/rs/xid"
	"golang.org/x/crypto/bcrypt"
)

type Account struct {
	id             string
	email          string
	hashedPassword []byte
	fullName       string
	address        string
	phoneNumber    string
	googleID       string
}

func NewAccount(id, email string, hashedPassword []byte, fullName, address, phoneNumber, googleID string) (*Account, error) {
	if id == "" {
		return nil, core.ValidationError("empty id")
	}

	if email == "" {
		return nil, core.ValidationError("empty email")
	}

	return &Account{
		id:             id,
		email:          email,
		hashedPassword: hashedPassword,
		fullName:       fullName,
		address:        address,
		phoneNumber:    phoneNumber,
		googleID:       googleID,
	}, nil
}

func (acc Account) ID() string {
	return acc.id
}

func CreateAccountWithEmail(id, email, password string) (*Account, error) {
	if password == "" {
		return nil, core.ValidationError("empty password")
	}

	hashedPwd, err := hashPassword(password)
	if err != nil {
		return nil, err
	}

	return NewAccount(id, email, hashedPwd, "", "", "", "")
}

func CreateAccountWithGoogleID(id, email, googleID string) (*Account, error) {
	if googleID == "" {
		return nil, core.ValidationError("empty googleID")
	}

	return NewAccount(id, email, nil, "", "", "", googleID)
}

func (acc Account) Email() string {
	return acc.email
}

func (acc *Account) ChangeEmail(email string) error {
	if email == "" {
		return core.ValidationError("empty email")
	}

	// TODO: validate email format

	acc.email = email
	return nil
}

func (acc Account) HashedPassword() []byte {
	return acc.hashedPassword
}

func (acc *Account) ChangePassword(password string) error {
	if password == "" {
		return core.ValidationError("empty password")
	}

	hashedPwd, err := hashPassword(password)
	if err != nil {
		return err
	}

	acc.hashedPassword = hashedPwd
	return nil
}

func (acc Account) FullName() string {
	return acc.fullName
}

func (acc *Account) ChangeFullName(fullName string) error {
	if fullName == "" {
		return core.ValidationError("empty fullName")
	}

	acc.fullName = fullName
	return nil
}

func (acc Account) Address() string {
	return acc.address
}

func (acc *Account) ChangeAddress(address string) error {
	if address == "" {
		return core.ValidationError("empty address")
	}

	acc.address = address
	return nil
}

func (acc Account) PhoneNumber() string {
	return acc.phoneNumber
}

func (acc *Account) ChangePhoneNumber(phoneNumber string) error {
	if phoneNumber == "" {
		return core.ValidationError("empty phoneNumber")
	}

	acc.phoneNumber = phoneNumber
	return nil
}

func (acc Account) GoogleID() string {
	return acc.googleID
}

func hashPassword(s string) ([]byte, error) {
	return bcrypt.GenerateFromPassword([]byte(s), bcrypt.MinCost)
}

type ProfileAttributes struct {
	FullName    string
	Address     string
	PhoneNumber string
	Email       string
}

func NextAccountID() string {
	return xid.New().String()
}

// AccountRepository represents account repository.
//go:generate mockery -name=AccountRepository
type AccountRepository interface {
	Store(*Account) error
	AccountByID(id string) (*Account, error)
	AccountByEmail(email string) (*Account, error)
	Update(*Account) error
}
