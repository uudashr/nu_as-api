package user_test

import (
	"testing"

	"golang.org/x/crypto/bcrypt"
)

func TestBcrypt(t *testing.T) {
	password1 := "Hello World!"
	password2 := "Hello World!"

	hashedPwd1, err := bcrypt.GenerateFromPassword([]byte(password1), bcrypt.MinCost)
	if err != nil {
		t.Fatal("err:", err)
	}

	hashedPwd2, err := bcrypt.GenerateFromPassword([]byte(password2), bcrypt.MinCost)
	if err != nil {
		t.Fatal("err:", err)
	}

	if string(hashedPwd1) == string(hashedPwd2) {
		t.Fatal("We got same hashed password")
	}
}

func TestPasswordReset_Expiry(t *testing.T) {
	// TODO: test expiry
}
